public class Covoiturage
{
	Voiture[] voitures;
	Personne[] personnes;
	
	/* Constructeur */
	Covoiturage(Voiture[] voitures, Personne[] personnes)
	{
		this.voitures = voitures;
		this.personnes = personnes;
	}
	
	/* Méthodes publiques */
	public boolean villeDesservie(String ville)
	{
		int i = 0;
		boolean desservie = false;
		
		while (i < this.voitures.length && !desservie)
		{
			if (this.voitures[i].ville == ville)
				desservie = true;
			i++;
		}
				
		return desservie;
	}
	
	public int nbPersonnes(String ville)
	{
		int nb = 0;
		for (int i = 0; i < this.personnes.length; i++)
			if (this.personnes[i].ville == ville)
				nb++;
				
		return nb;
	}
	
	public boolean capaciteSuffisante(String ville)
	{	
		int nb_personnes = 0;
		int nb_places = 0;
		
		for (int i = 0; i < this.voitures.length; i++)
			if (this.voitures[i].ville == ville)
				nb_places += this.voitures[i].capa;
				
		for (int i = 0; i < this.personnes.length; i++)
			if (this.personnes[i].ville == ville)
				nb_personnes++;
				
		return nb_personnes <= nb_places;
	}
	
	public boolean estVilleDans(String[] villes, String ville)
	{
		boolean found = false;
		int i = 0;
		
		while (i < villes.length && !found)
		{
			if (villes[i] == ville)
				found = true;
			i++;
		}
				
		return found;
	}
	
	public String[] getVilles()
	{
		String[] villes = new String[this.personnes.length + this.voitures.length];
		
		int j = 0;
		
		for (int i = 0; i < this.personnes.length; i++)
		{
			if (!estVilleDans(villes, this.personnes[i].ville))
			{
				villes[j] = this.personnes[i].ville;
				j++;
			}
		}
		
		for (int i = 0; i < this.voitures.length; i++)
		{
			if (!estVilleDans(villes, this.voitures[i].ville))
			{
				villes[j] = this.voitures[i].ville;
				j++;
			}
		}
		
		return copieTableauS(villes, j);
		//return Arrays.copyOf(villes, j);
	}
	
	public boolean capaciteSuffisante()
	{
		String[] villes = getVilles();
		boolean capa = true;
		int i = 0;
		
		if (villes.length > 0)
		{
			while (i < villes.length && capa)
			{	
				if (!capaciteSuffisante(villes[i]))
					capa = false;
				i++;
			}
		}
		else
			capa = false;
		
		return capa;
	}
	
	public boolean estPossible()
	{
		boolean possible = false;
		
		if (capaciteSuffisante())
		{
			//trier voiture par capacité -> faire tri fusion
			Voiture[] voitures_triees = new Voiture[this.voitures.length];
			voitures_triees = copieTableauV(this.voitures, this.voitures.length);
			//voitures_triees = Arrays.copyOf(this.voitures, this.voitures.length);
			//Arrays.sort(voitures_triees, new VoitureCapaciteComparator());
			voitures_triees = triFusionV(voitures_triees);
			
			int nb_personnes = this.personnes.length;
			int nb_veh = 0;
			int remplir = 0;
			int i = 0;
			
			while (i < voitures_triees.length && remplir < nb_personnes)
			{
				remplir += (nb_personnes - voitures_triees[i].capa) < 0 ? nb_personnes : voitures_triees[i].capa;
				nb_veh++;
				i++;
			}
			
			int nb_conducteur = 0;
			for (int j = 0; j < this.personnes.length; j++)
				if (this.personnes[j].peutConduire())
					nb_conducteur++;
			
			if (nb_veh - nb_conducteur <= 0)
				possible = true;
		}
		
		return possible;
	}
	
	public int getIdentifiant(String nomPersonne)
	{
		int id = -1;
		
		int i = 0;
		
		while (i < this.personnes.length && id == -1)
		{
			if (this.personnes[i].nom == nomPersonne)
			{
				id = this.personnes[i].id;
			}
			i++;
		}
				
		return id;
	}
	
	public int[] Attribution()
	{
		int[] attrib = new int[this.personnes.length];
		
		//trier voiture par capacité -> faire tri fusion
		Voiture[] voitures_triees = new Voiture[this.voitures.length];
		voitures_triees = copieTableauV(this.voitures, this.voitures.length);
		//voitures_triees = Arrays.copyOf(this.voitures, this.voitures.length);
		//Arrays.sort(voitures_triees, new VoitureCapaciteComparator());
		voitures_triees = triFusionV(voitures_triees);
		
		int k = 0;
		int i = 0;
		while (i < voitures_triees.length && k < attrib.length)
		{
			int j = 0;
			while (j < voitures_triees[i].capa && k < attrib.length)
			{
				attrib[k] = voitures_triees[i].id;
				k++;
				j++;
			}
			i++;
		}
		
		return attrib;
	}
	
	public Personne[] triePersonnes()
	{
		//trier personnes par nom -> faire tri fusion
		Personne[] personnes_triees = new Personne[this.personnes.length];
		personnes_triees = copieTableauP(this.personnes, this.personnes.length);
		//personnes_triees = Arrays.copyOf(this.personnes, this.personnes.length);
		//Arrays.sort(personnes_triees, new PersonneNomComparator());		
		personnes_triees = triFusionP(personnes_triees);
		
		this.personnes = copieTableauP(personnes_triees, personnes_triees.length);
		//this.personnes = Arrays.copyOf(personnes_triees, personnes_triees.length);
		
		return personnes_triees;
	}
	
	public int getIdentifiantDichotomique(String nomPersonne)
	{
		Personne[] ptriees = triePersonnes();
		
		int id = -1;
		int inf = 0;
		int sup = ptriees.length - 1;
		
		Personne p = new Personne(0, nomPersonne, "", false);
			
		while (inf <= sup && id == -1)
		{
			int mil = (inf + sup) / 2;
			
			int r = p.compare(ptriees[mil]);
			//int r = pnc.compare(new Personne(0, nomPersonne, "", false), ptriees[mil]);
			
			if (r < 0)
			{
				sup = mil - 1;
			}
			else
			{
				if (r > 0)
				{
					inf = mil + 1;
				}
				else
					if (r == 0)
						id = ptriees[mil].id;
			}
		}
		
		return id;
	}
	
	/* Méthodes privées */
	private String[] copieTableauS(String[] tabSource, int length)
	{
		return copieTableauS(tabSource, 0, length - 1);
	}
	
	private String[] copieTableauS(String[] tabSource, int start, int end)
	{
		String[] tabDest = new String[end - start + 1];
		
		for (int i = start; i <= end; i++)
		{
			tabDest[i - start] = tabSource[i];
		}
		
		return tabDest;
	}
	
	private Voiture[] copieTableauV(Voiture[] tabSource, int length)
	{
		return copieTableauV(tabSource, 0, length - 1);
	}
	
	private Voiture[] copieTableauV(Voiture[] tabSource, int start, int end)
	{
		Voiture[] tabDest = new Voiture[end - start + 1];
		
		for (int i = start; i <= end; i++)
		{
			tabDest[i - start] = tabSource[i];
		}
		
		return tabDest;
	}
	
	private Personne[] copieTableauP(Personne[] tabSource, int length)
	{
		return copieTableauP(tabSource, 0, length - 1);
	}
	
	private Personne[] copieTableauP(Personne[] tabSource, int start, int end)
	{
		Personne[] tabDest = new Personne[end - start + 1];
		
		for (int i = start; i <= end; i++)
		{
			tabDest[i - start] = tabSource[i];
		}
		
		return tabDest;
	}
	
	
	private Voiture[] fusionV(Voiture[] tab1, Voiture[] tab2)
	{
		Voiture[] ret = new Voiture[tab1.length + tab2.length];
		
		int i = 0, j = 0, k = 0;
		
		while (j < tab1.length && k < tab2.length)
		{
			if (tab1[j].compare(tab2[k]) >= 0)
			{
				ret[i] = tab1[j];
				j++;
			}
			else
			{
				ret[i] = tab2[k];
				k++;
			}
			
			i++;
		}
		
		while(j < tab1.length)
		{
			ret[i] = tab1[j];
			i++;
			j++;
		}
		
		while(k < tab2.length)
		{
			ret[i] = tab2[k];
			i++;
			k++;
		}
		
		return ret;
	}
	
	private Personne[] fusionP(Personne[] tab1, Personne[] tab2)
	{
		Personne[] ret = new Personne[tab1.length + tab2.length];
		
		int i = 0, j = 0, k = 0;
		
		while (j < tab1.length && k < tab2.length)
		{
			if (tab1[j].compare(tab2[k]) <= 0)
			{
				ret[i] = tab1[j];
				j++;
			}
			else
			{
				ret[i] = tab2[k];
				k++;
			}
			
			i++;
		}
		
		while(j < tab1.length)
		{
			ret[i] = tab1[j];
			i++;
			j++;
		}
		
		while(k < tab2.length)
		{
			ret[i] = tab2[k];
			i++;
			k++;
		}
		
		return ret;
	}
	
	private Voiture[] triFusionV(Voiture[] tab)
	{
		Voiture[] ret = new Voiture[tab.length];
		
		if (tab.length <= 1)
			ret = tab;
		else
		{
			int mil = tab.length / 2;
			Voiture[] tab1 = copieTableauV(tab, 0, mil - 1);
			Voiture[] tab2 = copieTableauV(tab, mil, tab.length - 1);
			ret = fusionV(triFusionV(tab1), triFusionV(tab2));
		}
		
		return ret;
	}
	
	private Personne[] triFusionP(Personne[] tab)
	{
		Personne[] ret = new Personne[tab.length];
		
		if (tab.length <= 1)
			ret = tab;
		else
		{
			int mil = tab.length / 2;
			Personne[] tab1 = copieTableauP(tab, 0, mil - 1);
			Personne[] tab2 = copieTableauP(tab, mil, tab.length - 1);
			ret = fusionP(triFusionP(tab1), triFusionP(tab2));
		}
		
		return ret;
	}
}