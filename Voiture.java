public class Voiture
{
	public int id;
	public String ville;
	public int capa;
	
	/* Constructeur */
	public Voiture(int id, String ville, int capa)
	{
		this.id = id;
		this.ville = ville;
		this.capa = capa;
	}
	
	/* Méthodes publiques */
	public int compare(Voiture other)
	{
		if (other == null)
			return 0;
			
		return (this.capa < other.capa) ? 1 : (this.capa > other.capa) ? -1 : 0;
	}
}