public class Executable
{
	public static void main(String[] args)
	{
		Voiture v1=new Voiture(1,"Paris",5);
		Voiture v2=new Voiture(2,"Marseille",3);
		Personne p1=new Personne(1,"Toto","Paris",true);
		Personne p2=new Personne(2,"Titi","Paris",true);
		Personne p3=new Personne(3, "Abra", "Swagville", false);
 		Covoiturage c1=new Covoiturage(new Voiture[]{v1,v2},new Personne[]{p1,p2, p3} ) ;
        
		System.out.println("Voiture " + v1.id + " peut prendre " + v1.capa + " personnes et stationne à  " + v1.ville);
		System.out.println("Personne "+ p1.id + " s'appelle " + p1.nom + " et habite à " + p1.ville);
		System.out.println("Marseille est desservie : " + c1.villeDesservie("Marseille"));
		System.out.println("Le nombre d'étudiants habitant à Merseille est " + c1.nbPersonnes("Marseille"));
		
		// RAJOUTEZ VOS PROPRES EXEMPLES
		int[] attrib = c1.Attribution();
		for (int i = 0; i < attrib.length; i++)
			System.out.println("attrib[" + i + "] = " + attrib[i]);
			
		Personne[] ptriees = c1.triePersonnes();
			
		for (Personne p : ptriees)
			System.out.println(p.nom + " id = " + c1.getIdentifiantDichotomique(p.nom));
    }
}