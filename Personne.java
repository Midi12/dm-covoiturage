public class Personne
{
	public int id;
	public String nom;
	public String ville;
	public boolean permis;
	
	/* Constructeur */
	public Personne(int id, String nom, String ville, boolean permis)
	{
		this.id = id;
		this.nom = nom;
		this.ville = ville;
		this.permis = permis;
	}
	
	/* Méthodes publiques */
	public boolean peutConduire()
	{
		return this.permis;
	}
	
	public int compare(Personne other)
	{
		if (other == null)
			return 0;
		
		String nom_p = this.nom.toLowerCase();
		String nom_other = other.nom.toLowerCase();
		
		int pos = 0;
		int i = 0;
		boolean swapped = false;
		
		// assure la plus petite longueur pour l'iteration
		if (nom_p.length() > nom_other.length())
		{
			String tmp = nom_other;
			nom_other = nom_p;
			nom_p = tmp;
			swapped = true;
		}
		
		while (i < nom_p.length() && pos == 0)
		{
			if ((char)nom_p.charAt(i) < (char)nom_other.charAt(i))
				pos = -1;
			else
				if ((char)nom_p.charAt(i) > (char)nom_other.charAt(i))
					pos = 1;
			i++;
		}
		
		if (pos == 0)
		{
			if (nom_p.length() < nom_other.length())
				pos = -1;
			else
				if (nom_p.length() > nom_other.length())
					pos = 1;
					
			if (swapped)
				pos *= -1;
		}
		
		return pos;
	}
}